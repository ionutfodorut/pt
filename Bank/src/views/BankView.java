/*
 * Created by JFormDesigner on Tue May 09 01:41:07 EEST 2017
 */

package views;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * @author Ioan Fodorut
 */
public class BankView extends JFrame {



    public BankView() {
        initComponents();
    }

    private void createUIComponents() {
        // TODO: add custom component creation code here
    }

    public void addAddPersonListener(ActionListener listenForAddPersonButton){
        addPerson.addActionListener(listenForAddPersonButton);
    }

    public void addRemovePersonListener(ActionListener listenForClearButton){
        removePerson.addActionListener(listenForClearButton);
    }

    public void addShowPersonListener(ActionListener listenForShowPersonButton){
        showPerson.addActionListener(listenForShowPersonButton);
    }

    public void addEditPersonListener(ActionListener listenForEditPersonButton){
        editPerson.addActionListener(listenForEditPersonButton);
    }

    public void addShowAccountsListener(ActionListener listenForShowAccountsButton) {
        showAccounts.addActionListener(listenForShowAccountsButton);
    }

    public void addAddAccountListener(ActionListener listenForAddAccountButton){
        addAccount.addActionListener(listenForAddAccountButton);
    }

    public void addRemoveAccountListener(ActionListener listenForRemoveAccountButton){
        removeAccount.addActionListener(listenForRemoveAccountButton);
    }

    public void addShowBalanceListener(ActionListener listenForShowBalanceButton){
        showBalance.addActionListener(listenForShowBalanceButton);
    }

    public void addDepositListener(ActionListener listenForDepositButton){
        deposit.addActionListener(listenForDepositButton);
    }

    public void addWithdrawListener(ActionListener listenForWithdrawButton){
        withdraw.addActionListener(listenForWithdrawButton);
    }



    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Ioan Fodorut
        label1 = new JLabel();
        Title = new JLabel();
        person = new JLabel();
        message = new JLabel();
        addPerson = new JButton();
        SSN = new JLabel();
        scrollPane2 = new JScrollPane();
        ssn = new JTextArea();
        removePerson = new JButton();
        E_mail = new JLabel();
        scrollPane3 = new JScrollPane();
        email = new JTextArea();
        showPerson = new JButton();
        Telephone = new JLabel();
        scrollPane4 = new JScrollPane();
        telephone = new JTextArea();
        editPerson = new JButton();
        Name = new JLabel();
        scrollPane5 = new JScrollPane();
        name = new JTextArea();
        showAccounts = new JButton();
        account = new JLabel();
        panel3 = new JPanel();
        addAccount = new JButton();
        removeAccount = new JButton();
        Account_id = new JLabel();
        scrollPane6 = new JScrollPane();
        accountId = new JTextArea();
        showBalance = new JButton();
        Sum = new JLabel();
        scrollPane7 = new JScrollPane();
        sum = new JTextArea();
        panel2 = new JPanel();
        withdraw = new JButton();
        deposit = new JButton();
        Type = new JLabel();
        panel1 = new JPanel();
        savings = new JRadioButton();
        spending = new JRadioButton();
        clear = new JButton();
        scrollPane1 = new JScrollPane();

        type.add(savings);
        type.add(spending);

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new GridLayoutManager(14, 5, new Insets(0, 0, 0, 0), -1, -1));

        //---- label1 ----
        label1.setIcon(new ImageIcon(getClass().getResource("/views/Untitled-1.png")));
        contentPane.add(label1, new GridConstraints(0, 0, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- Title ----
        Title.setText("Bank of Ionut");
        contentPane.add(Title, new GridConstraints(0, 1, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- person ----
        person.setText("Personal Information");
        contentPane.add(person, new GridConstraints(1, 0, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- message ----
        message.setText("Hello and welcome!");
        contentPane.add(message, new GridConstraints(1, 1, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- addPerson ----
        addPerson.setText("Add person");
        contentPane.add(addPerson, new GridConstraints(1, 2, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- SSN ----
        SSN.setText("SSN");
        contentPane.add(SSN, new GridConstraints(2, 0, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //======== scrollPane2 ========
        {

            //---- ssn ----
            ssn.setPreferredSize(new Dimension(200, 17));
            scrollPane2.setViewportView(ssn);
        }
        contentPane.add(scrollPane2, new GridConstraints(2, 1, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- removePerson ----
        removePerson.setText("Remove person");
        contentPane.add(removePerson, new GridConstraints(2, 2, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- E_mail ----
        E_mail.setText("E-mail");
        contentPane.add(E_mail, new GridConstraints(3, 0, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //======== scrollPane3 ========
        {

            //---- email ----
            email.setPreferredSize(new Dimension(200, 17));
            scrollPane3.setViewportView(email);
        }
        contentPane.add(scrollPane3, new GridConstraints(3, 1, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- showPerson ----
        showPerson.setText("Show personal information");
        contentPane.add(showPerson, new GridConstraints(3, 2, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- Telephone ----
        Telephone.setText("Telephone");
        contentPane.add(Telephone, new GridConstraints(4, 0, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //======== scrollPane4 ========
        {

            //---- telephone ----
            telephone.setPreferredSize(new Dimension(200, 17));
            scrollPane4.setViewportView(telephone);
        }
        contentPane.add(scrollPane4, new GridConstraints(4, 1, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- editPerson ----
        editPerson.setText("Edit personal information");
        contentPane.add(editPerson, new GridConstraints(4, 2, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- Name ----
        Name.setText("Name");
        contentPane.add(Name, new GridConstraints(5, 0, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //======== scrollPane5 ========
        {

            //---- name ----
            name.setPreferredSize(new Dimension(200, 17));
            scrollPane5.setViewportView(name);
        }
        contentPane.add(scrollPane5, new GridConstraints(5, 1, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- showAccounts ----
        showAccounts.setText("Show personal accounts");
        contentPane.add(showAccounts, new GridConstraints(5, 2, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- account ----
        account.setText("Account Information");
        contentPane.add(account, new GridConstraints(7, 0, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //======== panel3 ========
        {

            // JFormDesigner evaluation mark
            panel3.setBorder(new javax.swing.border.CompoundBorder(
                new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                    "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
                    javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                    java.awt.Color.red), panel3.getBorder())); panel3.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

            panel3.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));

            //---- addAccount ----
            addAccount.setText("Add account");
            panel3.add(addAccount, new GridConstraints(0, 0, 1, 1,
                GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                null, null, null));

            //---- removeAccount ----
            removeAccount.setText("Remove Account");
            panel3.add(removeAccount, new GridConstraints(0, 1, 1, 1,
                GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                null, null, null));
        }
        contentPane.add(panel3, new GridConstraints(7, 2, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- Account_id ----
        Account_id.setText("Account ID");
        contentPane.add(Account_id, new GridConstraints(8, 0, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //======== scrollPane6 ========
        {

            //---- accountId ----
            accountId.setPreferredSize(new Dimension(200, 17));
            scrollPane6.setViewportView(accountId);
        }
        contentPane.add(scrollPane6, new GridConstraints(8, 1, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- showBalance ----
        showBalance.setText("Show Balance");
        contentPane.add(showBalance, new GridConstraints(8, 2, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- Sum ----
        Sum.setText("Sum");
        contentPane.add(Sum, new GridConstraints(9, 0, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //======== scrollPane7 ========
        {

            //---- sum ----
            sum.setPreferredSize(new Dimension(200, 17));
            scrollPane7.setViewportView(sum);
        }
        contentPane.add(scrollPane7, new GridConstraints(9, 1, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //======== panel2 ========
        {
            panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));

            //---- withdraw ----
            withdraw.setText("Withdraw");
            panel2.add(withdraw, new GridConstraints(0, 0, 1, 1,
                GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                null, null, null));

            //---- deposit ----
            deposit.setText("Deposit");
            panel2.add(deposit, new GridConstraints(0, 1, 1, 1,
                GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                null, null, null));
        }
        contentPane.add(panel2, new GridConstraints(9, 2, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- Type ----
        Type.setText("Type");
        contentPane.add(Type, new GridConstraints(10, 0, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //======== panel1 ========
        {
            panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));

            //---- savings ----
            savings.setText("Savings Account");
            panel1.add(savings, new GridConstraints(0, 0, 1, 1,
                GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                null, null, null));

            //---- spending ----
            spending.setText("Spending Account");
            panel1.add(spending, new GridConstraints(0, 1, 1, 1,
                GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                null, null, null));
        }
        contentPane.add(panel1, new GridConstraints(10, 1, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //---- clear ----
        clear.setText("Clear");
        contentPane.add(clear, new GridConstraints(10, 2, 1, 1,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(table1);
        }
        contentPane.add(scrollPane1, new GridConstraints(11, 0, 2, 3,
            GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
            null, null, null));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    public String getSsn() {
        return ssn.getText();
    }

    public String getEmail() {
        return email.getText();
    }

    public String getTelephone() {
        return telephone.getText();
    }

    public String getPersonName() {
        return name.getText();
    }

    public String getAccountId() {
        return accountId.getText();
    }

    public String getSum() {
        return sum.getText();
    }

    public boolean getSavings() {
        return savings.isSelected();
    }

    public boolean getSpending() {
        return spending.isSelected();
    }

    public void setMessage(String message) {
        this.message.setText(message);
    }

    public void setTable1(JTable table1) {
        this.table1 = table1;
    }

    public JTable getTable1() {
        return table1;
    }

    public DefaultTableModel getModel() {
        return model;
    }

    public void setModel(DefaultTableModel model) {
        this.model = model;
    }

    public void addRow(Object[] arr) {
        model.addRow(arr);
    }

    public void clearModel() {
        if (model.getRowCount() > 0) {
            for (int i = model.getRowCount() - 1; i > -1; i--) {
                model.removeRow(i);
            }
        }
    }

    private DefaultTableModel model = new DefaultTableModel(null, new Object[] {"Person ID", "Name", "E-mail", "Telephone", "Account ID", "Account Type", "Balance"});
    private JTable table1 = new JTable(model);
    private ButtonGroup type = new ButtonGroup();

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Ioan Fodorut
    private JLabel label1;
    private JLabel Title;
    private JLabel person;
    private JLabel message;
    private JButton addPerson;
    private JLabel SSN;
    private JScrollPane scrollPane2;
    private JTextArea ssn;
    private JButton removePerson;
    private JLabel E_mail;
    private JScrollPane scrollPane3;
    private JTextArea email;
    private JButton showPerson;
    private JLabel Telephone;
    private JScrollPane scrollPane4;
    private JTextArea telephone;
    private JButton editPerson;
    private JLabel Name;
    private JScrollPane scrollPane5;
    private JTextArea name;
    private JButton showAccounts;
    private JLabel account;
    private JPanel panel3;
    private JButton addAccount;
    private JButton removeAccount;
    private JLabel Account_id;
    private JScrollPane scrollPane6;
    private JTextArea accountId;
    private JButton showBalance;
    private JLabel Sum;
    private JScrollPane scrollPane7;
    private JTextArea sum;
    private JPanel panel2;
    private JButton withdraw;
    private JButton deposit;
    private JLabel Type;
    private JPanel panel1;
    private JRadioButton savings;
    private JRadioButton spending;
    private JButton clear;
    private JScrollPane scrollPane1;

    // JFormDesigner - End of variables declaration  //GEN-END:variables



}
