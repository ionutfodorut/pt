package controllers;

import model.Bank;
import utils.Serialization;
import views.BankView;

/**
 * Created by Ioan on 09.05.2017.
 */
public class MainController {

    public static void main(String[] args) {

        Bank bank = new Bank();
        Serialization.serialize(bank, "src/utils/bank.ser");
        BankView bankView = new BankView();
        BankController bankController = new BankController(bank, bankView);

    }

}
