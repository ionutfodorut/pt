package controllers;

import model.Account;
import model.Bank;
import model.Person;
import utils.AccountType;
import utils.Serialization;
import views.BankView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.Map;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

/**
 * Created by Ioan on 09.05.2017.
 */
public class BankController {

    private Bank bank;
    private BankView bankView;

    public BankController(Bank bank, BankView bankView) {

        this.bank = bank;
        this.bankView = bankView;
        this.bankView.setVisible(true);
        this.bankView.addAddPersonListener(new AddPersonListener());
        this.bankView.addRemovePersonListener(new RemovePersonListener());
        this.bankView.addShowPersonListener(new ShowPersonListener());
        this.bankView.addEditPersonListener(new EditPersonListener());
        this.bankView.addShowAccountsListener(new ShowAccountsListener());
        this.bankView.addAddAccountListener(new AddAccountListener());
        this.bankView.addRemoveAccountListener(new RemoveAccountListener());
        this.bankView.addShowBalanceListener(new ShowBalanceListener());
        this.bankView.addDepositListener(new DepositListener());
        this.bankView.addWithdrawListener(new WithdrawListener());

    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public BankView getBankView() {
        return bankView;
    }

    public void setBankView(BankView bankView) {
        this.bankView = bankView;
    }

    public class AddPersonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            bank = Serialization.deserialize("src/utils/bank.ser");
            bank.reestablishObservers();
            if (bankView.getSavings())
                bank.addPerson(parseInt(bankView.getSsn()), bankView.getEmail(), bankView.getPersonName(), bankView.getTelephone(), parseDouble(bankView.getSum()), parseInt(bankView.getAccountId()), AccountType.SAVINGS);
            if (bankView.getSpending())
                bank.addPerson(parseInt(bankView.getSsn()), bankView.getEmail(), bankView.getPersonName(), bankView.getTelephone(), parseDouble(bankView.getSum()), parseInt(bankView.getAccountId()), AccountType.SPENDING);
            Serialization.serialize(bank, "src/utils/bank.ser");
            //bankView.clearModel();
            for (Map.Entry<Person, LinkedList<Account>> entry : bank.getPersonAccountMap().entrySet()) {
                Person p = entry.getKey();
                LinkedList<Account> accounts = entry.getValue();
                for (Account account : accounts) {
                    bankView.addRow(new Object[]{ p.getId(), p.getName(), p.getEmail(), p.getTelephone(), account.getId(), account.getType(), account.getBalance()});
                }
            }
            bankView.setMessage("New person added to bank.");

        }
    }


    private class RemovePersonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            bank = Serialization.deserialize("src/utils/bank.ser");
            bank.reestablishObservers();
            Person p1 = bank.removePerson(parseInt(bankView.getSsn()));
            Serialization.serialize(bank, "src/utils/bank.ser");
            bankView.clearModel();
            if (p1 != null) {
                for (Map.Entry<Person, LinkedList<Account>> entry : bank.getPersonAccountMap().entrySet()) {
                    Person p = entry.getKey();
                    LinkedList<Account> accounts = entry.getValue();
                    for (Account account : accounts) {
                        getBankView().addRow(new Object[]{p.getId(), p.getName(), p.getEmail(), p.getTelephone(), account.getId(), account.getType(), account.getBalance()});
                    }
                }
                bankView.setMessage("Person removed from bank.");
            } else {
                bankView.setMessage("Inexistent person.");
            }
        }
    }


    private class ShowPersonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            bank = Serialization.deserialize("src/utils/bank.ser");
            bank.reestablishObservers();
            Person p = bank.getPerson(parseInt(bankView.getSsn()));
            if (p != null) {
                bankView.clearModel();
                bankView.addRow(new Object[]{p.getId(), p.getName(), p.getEmail(), p.getTelephone()});
                bankView.setMessage("Personal data shown in table below.");
            } else {
                bankView.setMessage("Inexistent person.");
            }
        }
    }

    private class EditPersonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            bank = Serialization.deserialize("src/utils/bank.ser");
            bank.reestablishObservers();
            LinkedList<Account> accounts = bank.getAccounts(parseInt(bankView.getSsn()));
            Person p = bank.getPerson(parseInt(bankView.getSsn()));
            if (p != null) {
                bank.editPerson(parseInt(bankView.getSsn()), bankView.getEmail(), bankView.getPersonName(), bankView.getTelephone());
                Serialization.serialize(bank, "src/utils/bank.ser");
                bankView.clearModel();
                for (Account account : accounts) {
                    bankView.addRow(new Object[]{bankView.getSsn(), bankView.getPersonName(), bankView.getEmail(), bankView.getTelephone(), account.getId(), account.getType(), account.getBalance()});
                }
                bankView.setMessage("Personal data of person shown below updated.");
            } else {
                bankView.setMessage("Inexistent person.");
            }
        }

    }

    private class ShowAccountsListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            bank = Serialization.deserialize("src/utils/bank.ser");
            bank.reestablishObservers();
            LinkedList<Account> accounts = bank.getAccounts(parseInt(bankView.getSsn()));
            Person p = bank.getPerson(parseInt(bankView.getSsn()));
            bankView.clearModel();
            if (p != null) {
                for (Account account : accounts) {
                    bankView.addRow(new Object[]{p.getId(), p.getName(), p.getEmail(), p.getTelephone(), account.getId(), account.getType(), account.getBalance()});

                }
                bankView.setMessage("Person shown in table below, with its accounts.");
            } else {
                bankView.setMessage("Inexistent person.");
            }

        }
    }

    private class AddAccountListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            bank = Serialization.deserialize("src/utils/bank.ser");
            bank.reestablishObservers();
            Person p = bank.getPerson(parseInt(bankView.getSsn()));
            if (p != null) {
                if (bankView.getSavings()) {
                    bank.addAccount(parseInt(bankView.getSsn()), parseDouble(bankView.getSum()), parseInt(bankView.getAccountId()), AccountType.SAVINGS);
                    bankView.clearModel();
                    bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                            bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                            bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                            bankView.getAccountId(), "Savings",
                            bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getBalance()});
                    bankView.setMessage("Savings account created.");

                } else if (bankView.getSpending()) {
                    bank.addAccount(parseInt(bankView.getSsn()), parseDouble(bankView.getSum()), parseInt(bankView.getAccountId()), AccountType.SPENDING);
                    bankView.clearModel();
                    bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                            bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                            bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                            bankView.getAccountId(), "Spending",
                            bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getBalance()});

                    bankView.setMessage("Spending account created.");
                } else {
                    bankView.setMessage("Please set an account type!");
                }
            } else {
                bankView.setMessage("Inexistent person.");
            }
            Serialization.serialize(bank, "src/utils/bank.ser");
        }
    }

    private class RemoveAccountListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            bank = Serialization.deserialize("src/utils/bank.ser");
            bank.reestablishObservers();
            Account account = bank.removeAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId()));
            if (account != null) {
                bankView.clearModel();
                if (account.getType().compareTo("Spending") == 0) {
                    bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                            bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                            bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                            bankView.getSsn(), "Spending",
                            account.getId(), account.getBalance()});
                } else {
                    bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                            bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                            bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                            bankView.getAccountId(), "Savings",
                           account.getId(), account.getBalance()});
                }
                bankView.setMessage("Account removed.");
            } else {
                bankView.setMessage("Inexistent account.");
            }
            Serialization.serialize(bank, "src/utils/bank.ser");
        }
    }

    private class ShowBalanceListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            bank = Serialization.deserialize("src/utils/bank.ser");
            bank.reestablishObservers();
            Person p = bank.getPerson(parseInt(bankView.getSsn()));
            if (p != null) {
                Account account = bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId()));
                if (account != null) {
                    if (account.getType().compareTo("Spending") == 0) {
                        bankView.clearModel();
                        bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                                bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                                bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                                account.getId(), "Spending",
                                account.getBalance()});
                    } else {
                        bankView.clearModel();
                        bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                                bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                                bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                                account.getId(), "Savings",
                                account.getBalance()});
                    }
                    bankView.setMessage("Balance shown in table below.");
                } else {
                    bankView.setMessage("Inexistent account.");
                }
            } else {
                bankView.setMessage("Inexistent person.");
            }
        }
    }

    private class DepositListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            bank = Serialization.deserialize("src/utils/bank.ser");
            bank.reestablishObservers();
            Person p = bank.getPerson(parseInt(bankView.getSsn()));
            if (p != null) {
                if (bank.deposit(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId()), parseDouble(bankView.getSum())) == -1) {
                    bankView.clearModel();
                    bankView.setMessage("Not enough money.");
                    if (bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getType().compareTo("Spending") == 0) {
                        bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                                bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                                bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                                bankView.getAccountId(), "Spending",
                                bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getBalance()});
                    } else {
                        bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                                bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                                bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                                bankView.getAccountId(), "Savings",
                                bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getBalance()});
                    }
                } else {
                    bankView.clearModel();
                    bankView.setMessage("Money deposited successfully.");
                    if (bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getType().compareTo("Spending") == 0) {
                        bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                                bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                                bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                                bankView.getAccountId(), "Spending",
                                bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getBalance()});
                    } else {
                        bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                                bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                                bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                                bankView.getAccountId(), "Savings",
                                bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getBalance()});
                    }
                }
            } else {
                bankView.setMessage("Inexistent person.");
            }
            Serialization.serialize(bank, "src/utils/bank.ser");
        }
    }

    private class WithdrawListener implements ActionListener {


        @Override
        public void actionPerformed(ActionEvent e) {
            bank = Serialization.deserialize("src/utils/bank.ser");
            bank.reestablishObservers();
            Person p = bank.getPerson(parseInt(bankView.getSsn()));
            if (p != null) {
                if (bank.withdraw(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId()), parseDouble(bankView.getSum())) == -1) {
                    bankView.setMessage("Not enough money in the account.");
                    bankView.clearModel();
                    if (bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getType().compareTo("Spending") == 0) {
                        bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                                bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                                bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                                bankView.getAccountId(), "Spending",
                                bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getBalance()});
                    } else {
                        bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                                bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                                bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                                bankView.getAccountId(), "Savings",
                                bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getBalance()});
                    }
                } else {
                    bankView.setMessage("Money withdrawn successfully");
                    bankView.clearModel();
                    if (bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getType().compareTo("Spending") == 0) {
                        bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                                bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                                bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                                bankView.getAccountId(), "Spending",
                                bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getBalance()});
                    } else {
                        bankView.addRow(new Object[]{bankView.getSsn(), bank.getPerson(parseInt(bankView.getSsn())).getName(),
                                bank.getPerson(parseInt(bankView.getSsn())).getEmail(),
                                bank.getPerson(parseInt(bankView.getSsn())).getTelephone(),
                                bankView.getAccountId(), "Savings",
                                bank.getAccount(parseInt(bankView.getSsn()), parseInt(bankView.getAccountId())).getBalance()});
                    }
                }
            } else {
                bankView.setMessage("Inexistent person.");
            }
            Serialization.serialize(bank, "src/utils/bank.ser");
        }
    }
}