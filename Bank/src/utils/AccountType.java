package utils;

/**
 * Created by Ioan on 09.05.2017.
 */
public enum AccountType {
    SAVINGS, SPENDING
}
