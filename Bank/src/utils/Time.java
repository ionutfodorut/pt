package utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ioan on 03.04.2017.
 * */
public class Time {
    public static int getTime() {
        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        Date dateobj = new Date();
        String time = df.format(dateobj);
        int timeInSecs = toSecs(time);
        return timeInSecs;
    }

    public static int toSecs(String s) {
        String[] hourMinSec = s.split(":");
        int hours = Integer.parseInt(hourMinSec[0]);
        int mins = Integer.parseInt(hourMinSec[1]);
        int secs = Integer.parseInt(hourMinSec[2]);
        return hours * 3600 + mins * 60 + secs;
    }

    public static String intToString (int i) {
        int hours = i / 3600;
        int mins = (i - hours*3600) / 60;
        int secs = i - hours*3600 - mins*60;
        return hours + ":" + mins + ":" + secs;
    }
}


