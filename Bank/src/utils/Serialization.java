package utils;

import model.Bank;

import java.io.*;

/**
 * Created by Ioan on 09.05.2017.
 */
public class Serialization {

    public static void serialize(Bank bank, String fileName) {
        try {
            FileOutputStream fileOut = new FileOutputStream(fileName);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(bank);
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved in " + fileName + "\n");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public static Bank deserialize(String fileName) {
        Bank bank = null;
        try {
            FileInputStream fileIn = new FileInputStream(fileName);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            bank = (Bank) in.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        return bank;
    }
}
