package model;

import utils.Time;

/**
 * Created by Ioan on 08.05.2017.
 */
public class SavingsAccount extends Account {

    private int timeOfDeposit;
    private int timeOfWithdrawal;
    private double interestRate;

    public SavingsAccount(double balance, int id) {
        super(balance, id);
        this.timeOfDeposit = 0;
        this.timeOfDeposit = 0;
        this.interestRate = 0.05;
    }

    @Override
    protected double deposit(double sum) {

        if (super.getBalance() + sum < 0)
            return -1;

        super.setBalance(super.getBalance() + sum);
        this.timeOfDeposit = Time.getTime();
        this.setChanged();
        this.notifyObservers("Deposited " + sum + " to savings account ");
        return super.getBalance();
    }

    @Override
    protected double withdraw(double sum) {
        if (super.getBalance() - sum < 0) {
            return -1;
        }

        this.timeOfWithdrawal = Time.getTime();
        double interest = super.getBalance() * (timeOfWithdrawal - timeOfDeposit) / (3600*24*356) * interestRate;
        double finalSum = super.getBalance() + interest;
        super.setBalance(0);
        this.setChanged();
        this.notifyObservers("Withdrawn " + sum + " from savings account ");
        return finalSum;
    }

    public String getType() {
        return "Savings";
    }

}
