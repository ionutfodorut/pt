package model;

/**
 * Created by Ioan on 25.04.2017.
 */
public class Product {
    private String name;
    private int price;
    private int stock;
    private int product_id;

    public Product(String name, int price, int stock, int product_id) {
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }
}
