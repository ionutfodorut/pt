package model;

/**
 * Created by Ioan on 25.04.2017.
 */
public class Customer {

    private int customer_id;
    private String email;
    private int age;
    private String name;

    public Customer(){
        customer_id = 0;
        email = "no email";
        age = 0;
        name = "no name";
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
