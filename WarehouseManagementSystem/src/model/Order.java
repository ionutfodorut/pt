package model;

/**
 * Created by Ioan on 25.04.2017.
 */
public class Order {

    private int order_id;
    private int customer_id;

    public Order(int order_id, int customer_id) {
        this.order_id = order_id;
        this.customer_id = customer_id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }
}
