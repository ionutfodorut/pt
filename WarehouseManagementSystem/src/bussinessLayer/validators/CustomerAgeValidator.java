package bussinessLayer.validators;

import model.Customer;

/**
 * Created by Ioan on 07.05.2017.
 */
public class CustomerAgeValidator implements Validator<Customer> {

    public void validate(Customer t) {
        int age = t.getAge();
        if (age < 0) {
            throw new IllegalArgumentException("Age is not valid!");
        }
    }

}
