package controllers;

import model.IntegerMonomial;
import model.Monomial;
import model.Polynomial;
import model.RealMonomial;
import views.PolynomialView;

public class MainController{
        public static void main(String[]args){

        Polynomial polynomialx = new Polynomial();
        Polynomial polynomialy = new Polynomial();
        PolynomialView polynomialView = new PolynomialView();
        PolynomialController polynomialController = new PolynomialController(polynomialx, polynomialy, polynomialView);
        polynomialView.setVisible(true);


        //////////////////////////////////////////////Testing in the console\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        IntegerMonomial m1 = new IntegerMonomial(3, 3);
        IntegerMonomial m2 = new IntegerMonomial(3, 4);
        RealMonomial m3 = new RealMonomial(3, 2.3);
        RealMonomial m4 = new RealMonomial(3, 4.1);

        Monomial m5 = m1.add(m3);
        Monomial m6 = m2.add(m4);
        System.out.println("By adding " + m3 + " and " + m1 + " we obtain " + m5);
        System.out.println("By adding " + m2 + " and " + m4 + " we obtain " + m6);

        Polynomial polynomial = new Polynomial();
        polynomial.add(m1);
        polynomial.add(m2);
        polynomial.add(m3);
        System.out.println("Polynomial polynomial is " + polynomial + " after the addition of " + m1 + " " + m2 + " " + m3);
        
        Polynomial polynomial1 = new Polynomial();
        polynomial1.add(new RealMonomial(4, 3.2));
        polynomial1.add(new IntegerMonomial(2, 3));
        polynomial1.add(new RealMonomial(6, 13d));
        System.out.println("Polynomial polynomial1 is " + polynomial1);
        
        Polynomial polynomial2 = polynomial1.add(polynomial);
        System.out.println("polynomial2 = polynomial1 + polynomial = " + polynomial2 );

        Polynomial polynomial3 = polynomial1.subtract(polynomial);
        System.out.println("polynomial1 - polynomial = " + polynomial3);

        Polynomial polynomial4 = new Polynomial();
        polynomial4.add(new RealMonomial(4, 1.2));
        System.out.println("Polynomial polynomial4 is " + polynomial4);

        Polynomial polynomial5 = polynomial1.subtract(polynomial4);
        System.out.println("polynomial1 - polynomial4 = " + polynomial5);

        System.out.println("polynomial2 * polynomial1 = " + polynomial2.multiply(polynomial1));

        System.out.println("D(polynomial2) = " + polynomial2.differentiate());
        System.out.println("I(polynomail2) = " + polynomial2.integrate());

        Polynomial polynomial6 = new Polynomial();
        polynomial6.add(new RealMonomial(2, 1d));
        polynomial6.add(new RealMonomial(1, 1d));
        polynomial6.add(new RealMonomial(0, 1d));

        Polynomial polynomial7 = new Polynomial();
        polynomial7.add(new IntegerMonomial(3, 1));
        polynomial7.add(new IntegerMonomial(1, 1));
        polynomial7.add(new IntegerMonomial(0, 1));

        Polynomial polynomial8 = new Polynomial();
        polynomial8.add(new IntegerMonomial(1, 1));
        polynomial8.add(new IntegerMonomial(0, 1));
        System.out.println("Quotient is " + polynomial7.LongDivide(polynomial8)[0] +
                "\n" + "Remainder is " + polynomial7.LongDivide(polynomial8)[1]);
    }
}

