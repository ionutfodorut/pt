package utilities;

import exceptions.InvalidInputException;
import model.IntegerMonomial;
import model.Monomial;
import model.Polynomial;

import static java.lang.Integer.parseInt;

/**
 * Created by John on 13.03.2017.
 */
public class Parse {

    public static Polynomial parsePolynomial(String s) throws InvalidInputException {

            try{
                Polynomial polynomial = new Polynomial();
                String delims = "[+]";
                s.replaceAll(" ", "");
                s.replaceAll("x", "X");
                String[] tokens = s.split(delims);
                for (String z: tokens) {
                    String delims2 = "[X^]+";
                    String[] tokens2 = z.split(delims2);
                    if (tokens2.length > 1){
                        polynomial.add(new IntegerMonomial(parseInt(tokens2[1]),parseInt(tokens2[0])));
                    } else throw new InvalidInputException();

                }
                return polynomial;
            } catch (NumberFormatException e) {
                System.out.println("Invalid input!");
                throw new InvalidInputException();
            }

        }

}
