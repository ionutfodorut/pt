package utilities;

import model.Monomial;

import java.util.Comparator;

/**
 * Created by John on 07.03.2017.
 */
public class Sort implements Comparator<Monomial> {
    @Override
    public int compare(Monomial o1, Monomial o2) {
        return (o1.getDegree()- o2.getDegree());
    }
}
