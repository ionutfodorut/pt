package model;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class RealMonomial extends Monomial {
    public RealMonomial(int degree, Double coef) {
        this.degree = degree;
        this.coef = coef;
    }
    public Double getCoef() {
        return (Double) coef;
    }
    public void setCoef(Number coef) { this.coef = coef; }
    @Override
    public String toString() {
        //BigDecimal bd = new BigDecimal(d).setScale(2, RoundingMode.HALF_EVEN); -- I also tried to use this for formating
        DecimalFormat df = new DecimalFormat("#.#####");
        df.setRoundingMode(RoundingMode.CEILING);
        if (this.getDegree() == 0) {
            return "" + df.format(getCoef());
        } else if (this.getDegree() == 1) {
            if (this.getCoef() == 1) {
                return "X";
            } else if (getCoef() == -1) {
                return "(-X)";
            } else if (this.getCoef() < 0) {
                return "(" + df.format(getCoef()) + ")" + "X";
            } else
                return df.format(getCoef()) + "X";
        } else if (this.getDegree() > 1) {
            if (this.getCoef() == 1) {
                return "X^" + getDegree();
            } else if (getCoef() == -1) {
                return "(-X^" + getDegree() + ")";
            } else if (this.getCoef() < 0) {
                return "(" + df.format(getCoef())  + ")" + "X^" + getDegree();
            } else
                return df.format(getCoef()) + "X^" + getDegree();
        } return "";
    }
    @Override
    public Monomial add(Monomial m) {
        if (m instanceof IntegerMonomial) {
            RealMonomial m1 = new RealMonomial(this.degree, this.coef.doubleValue() + m.getCoef().doubleValue());
            return m1;
        }
        if (m instanceof RealMonomial) {
            RealMonomial m1 = new RealMonomial(this.degree, this.coef.doubleValue() + m.getCoef().doubleValue());
            return m1;
        }
        return null;
    }
    @Override
    public Monomial negate(){
        return new RealMonomial(this.degree, getCoef()*(-1));
    }
    @Override
    public Monomial multiply(Monomial m) {
       return new RealMonomial(m.degree + this.degree, m.getCoef().doubleValue() * this.getCoef());
    }
    @Override
    public Monomial divide(Monomial m) {
        return new RealMonomial(this.degree - m.getDegree(), this.getCoef() / m.getCoef().doubleValue());
    }
    @Override
    public Monomial integrate() {
        return new RealMonomial(this.degree + 1, this.getCoef() / (this.degree + 1));
    }
    @Override
    public Monomial differentiate() {
        return new RealMonomial(this.degree - 1, this.getCoef() * this.degree);
    }
}
