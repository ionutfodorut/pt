package model;




public class IntegerMonomial extends Monomial {
    public IntegerMonomial(int degree, Integer coef) { this.degree = degree; this.coef = coef; }
    public Integer getCoef(){
    	return (Integer) this.coef;
    }
	public void setCoef(Number coef) { this.coef = coef; }
    @Override
    public String toString() {
        if (this.getDegree() == 0) {
            return "" + getCoef();
        } else if (this.getDegree() == 1) {
            if (this.getCoef() == 1) {
                return "X";
            } else if (getCoef() == -1) {
                return "(-X)";
            } else if (this.getCoef() < 0) {
                return "(" + getCoef() + ")" + "X";
            } else
                return getCoef() + "X";
        } else if (this.getDegree() > 1) {
            if (this.getCoef() == 1) {
                return "X^" + getDegree();
            } else if (getCoef() == -1) {
                return "(-X^" + getDegree() + ")";
            } else if (this.getCoef() < 0) {
                return "(" + getCoef() + ")" + "X^" + getDegree();
            } else
                return getCoef() + "X^" + getDegree();
        }
        return "";
    }
    @Override
    public Monomial add(Monomial m) {
        if (m instanceof IntegerMonomial) {
            IntegerMonomial m1 = new IntegerMonomial(this.degree, (Integer) coef + m.getCoef().intValue());
            return m1;
        }
        if (m instanceof RealMonomial) {
        	RealMonomial m1 = new RealMonomial(this.degree, coef.doubleValue() + m.getCoef().doubleValue());
        	return m1;
        }
        return null;
    }
    @Override
    public Monomial negate(){
        return new IntegerMonomial(this.degree, getCoef()*(-1));
    }
    @Override
    public Monomial multiply(Monomial m) {
        return new IntegerMonomial(m.degree + this.degree, m.getCoef().intValue() * this.getCoef());
    }
    @Override
    public Monomial divide(Monomial m) {
        return new RealMonomial(this.degree - m.getDegree(), this.getCoef() / m.getCoef().doubleValue());
    }
    @Override
    public Monomial integrate() {
        return new RealMonomial(this.degree + 1, this.getCoef().doubleValue() / (this.degree + 1));
    }
    @Override
    public Monomial differentiate() {
        return new IntegerMonomial(this.degree - 1, this.getCoef() * this.degree);
    }
}
