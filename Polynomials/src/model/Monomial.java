package model;

import static java.lang.Math.abs;

public abstract class Monomial {
    protected int degree;
    protected Number coef;
    public int getDegree() {
        return degree;
    }
    public void setDegree(int degree) { this.degree = degree; }
    public abstract Number getCoef();
    public abstract void setCoef(Number coef);
    public abstract String toString();
    public boolean equals(Monomial m) {
        return (this.degree == m.getDegree());
    }
    public int compareTo(Monomial m) {
        return this.degree - m.getDegree();
    }
    public boolean checkIfZero(){
        return abs(this.getCoef().doubleValue()) < 0.001;
    }
    public abstract Monomial add(Monomial m);
    public abstract Monomial negate();
    public abstract Monomial integrate();
    public abstract Monomial differentiate();
    public abstract Monomial multiply(Monomial m);
    public abstract Monomial divide(Monomial m);
}

