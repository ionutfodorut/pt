package model;

import java.util.LinkedList;

public class Polynomial {
	private int degree;
    private LinkedList<Monomial> monomials;
    public Polynomial() {
        this.degree = 0;
        this.monomials = new LinkedList<>();
    }
    public int getDegree() {
        return degree;
    }
    public void setDegree(int degree) {
        this.degree = degree;
    }
    public LinkedList<Monomial> getMonomials() {
        return monomials;
    }
    public String toString() {
        String s = "";
        int i = 0;
        for (Monomial m: monomials){

            if (i == 0) {
                s = m.toString();
            } else {
                s = s + " + " + m.toString();
            }
            i++;
        }
        return s;
    }
    public Boolean equals(Polynomial polynomial) {
        return (this.monomials == polynomial.getMonomials()) && (this.degree == polynomial.getDegree());
    }
    public Polynomial clean(){
        Polynomial result = new Polynomial();
        for (Monomial mx: monomials){
            if (!mx.checkIfZero()){
                result.add(mx);
            }
        }
        return result;
    }
    public Polynomial add(Polynomial poly) {
        Polynomial result = new Polynomial();
        if (poly.getDegree() > this.degree) {
            result.setDegree(poly.getDegree());
        } else {
            result.setDegree(this.degree);
        }
        for (Monomial m: poly.getMonomials()) {
            result.add(m);
        }
        for (Monomial m: monomials) {
            result.add(m);
        }
        result = result.clean();
        return result;
    }
    public void add(Monomial m){
        if (m.getDegree() > this.degree) this.degree = m.getDegree();
        int i = 0;
        for (Monomial m1: monomials) {
            if (m.equals(m1)) {
                Monomial r = m1.add(m);
                monomials.remove(i);
                monomials.add(i, r);
                return;
            }
            if (m.compareTo(m1) > 0){
                monomials.add(i, m);
                return;
            }
            i++;
        }
        monomials.addLast(m);
    }
    public Polynomial subtract(Polynomial poly) {
        Polynomial result = new Polynomial();
        if (poly.getDegree() > this.degree) {
            result.setDegree(poly.getDegree());
        } else {
            result.setDegree(this.degree);
        }
        for (Monomial m: monomials) {
            result.add(m);
        }
        for (Monomial m: poly.getMonomials()) {
            result.add(m.negate());
        }
        result = result.clean();
        return result;
    }
    public Polynomial multiply(Polynomial poly) {
        Polynomial result = new Polynomial();
        result.setDegree(this.degree + poly.getDegree());
        for (Monomial m: monomials){
            for (Monomial p: poly.getMonomials()){
                result.add(m.multiply(p));
            }
        }
        result = result.clean();
        return result;
    }
    public Polynomial[] LongDivide(Polynomial poly) {
        Polynomial quotient = new Polynomial(), remainder, dividend;
        dividend = this;
        int i = 0;
        if (dividend.getMonomials().size() > 1) {
            while (dividend.getMonomials().get(0).getDegree() >= poly.getMonomials().get(0).getDegree()){
                quotient.add(dividend.getMonomials().get(0).divide(poly.getMonomials().get(0)));
                Polynomial aux = new Polynomial();
                if (quotient.getMonomials().size() >= i) {
                    aux.add(quotient.getMonomials().get(i));
                }
                remainder = poly.multiply(aux);
                dividend = dividend.subtract(remainder);
                dividend = dividend.clean();
                i++;
            }
        }
        return new Polynomial[] {quotient, dividend};
    }
    public Polynomial[] SyntheticDivide(Polynomial poly) {
        Polynomial result = this;
        double normalizer = this.monomials.getLast().getCoef().doubleValue();

        for (int i = this.monomials.size() - (poly.getMonomials().size() - 1); i > 0 ; i--) {
            result.getMonomials().get(i).divide(new RealMonomial(0, normalizer));
            Number coef = result.getMonomials().get(i).getCoef();
            for (int j = poly.getMonomials().size() - 1; j > 1; j--)
                result.getMonomials().get(i+j).add(poly.getMonomials().get(j).negate().multiply(new RealMonomial(0, coef.doubleValue())));
        }

        int separator = result.getMonomials().size() - (poly.getMonomials().size()- 1);

        Polynomial quotient = new Polynomial();
        for (Monomial m: result.getMonomials().subList(0, separator - 1)) {
            quotient.add(m);
        }
        Polynomial remainder = new Polynomial();
        for (Monomial m: result.getMonomials().subList(separator, result.getMonomials().size())) {
            remainder.add(m);
        }
        //System.out.println("Result is " + quotient);
        return new Polynomial[] {quotient, remainder};
    }
    public Polynomial integrate() {
        Polynomial result = new Polynomial();
        result.setDegree(this.degree + 1);
        for (Monomial m: monomials) {
            result.add(m.integrate());
        }
        return result;
    }
    public Polynomial differentiate() {
        Polynomial result = new Polynomial();
        result.setDegree(this.degree - 1);
        for (Monomial m: monomials) {
            result.add(m.differentiate());
        }
        result = result.clean();
        return result;
    }
}
