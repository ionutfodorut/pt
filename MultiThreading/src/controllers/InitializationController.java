package controllers;

import views.InitializationView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static java.lang.Integer.parseInt;

/**
 * Created by Ioan on 03.04.2017.
 */

public class InitializationController {

    private InitializationView initializationView;
    private int totalSimulationTime;
    private int minArrivalInterval;
    private int maxArrivalInterval;
    private int minServiceTime;
    private int maxServiceTime;
    private int nrOfClients;
    private int nrOfServers;
    private boolean dataGathered = false;

    public InitializationController() {
        initializationView = new InitializationView();
        initializationView.setVisible(true);
        initializationView.addClearListener(new ClearListener());
        initializationView.addStartListener(new StartListener());
    }

    public class ClearListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            initializationView.clear();
        }
    }

    public class StartListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                if (parseInt(initializationView.getTotalSimulationTime()) >= 0
                        && parseInt(initializationView.getMinArrivalInterval()) >= 0
                        && parseInt(initializationView.getMaxArrivalInterval()) >= 0
                        && parseInt(initializationView.getMinServiceTime()) >= 0
                        && parseInt(initializationView.getMaxServiceTime()) >= 0
                        && parseInt(initializationView.getNrOfClients()) >= 0
                        && parseInt(initializationView.getNrOfServers()) >= 0
                        && parseInt(initializationView.getMaxArrivalInterval()) >= parseInt(initializationView.getMinArrivalInterval())
                        && parseInt(initializationView.getMaxServiceTime()) >= parseInt(initializationView.getMinServiceTime())) {

                        setTotalSimulationTime(parseInt(initializationView.getTotalSimulationTime()));
                        setMinArrivalInterval(parseInt(initializationView.getMinArrivalInterval()));
                        setMaxArrivalInterval(parseInt(initializationView.getMaxArrivalInterval()));
                        setMinServiceTime(parseInt(initializationView.getMinServiceTime()));
                        setMaxServiceTime(parseInt(initializationView.getMaxServiceTime()));
                        setNrOfClients(parseInt(initializationView.getNrOfClients()));
                        setNrOfServers(parseInt(initializationView.getNrOfServers()));
                        initializationView.setMessage("Information gathered successfully.");
                        setDataGathered(true);
                } else {
                    initializationView.setMessage("Error! Incorrect or incomplete information. Please try again.");
                    setDataGathered(false);
                }
            } catch (NumberFormatException ex) {
                initializationView.setMessage("Error! Incorrect or incomplete information. Please try again.");
                setDataGathered(false);
            }
        }
    }

    public void setInitializationView(boolean b) {
        this.initializationView.setVisible(b);
    }
    private void setDataGathered(boolean b) {
        dataGathered = b;
    }
    public boolean isDataGathered() {
        return dataGathered;
    }
    public int getTotalSimulationTime() {
        return totalSimulationTime;
    }
    public void setTotalSimulationTime(int totalSimulationTime) {
        this.totalSimulationTime = totalSimulationTime;
    }
    public int getMinArrivalInterval() {
        return minArrivalInterval;
    }
    public void setMinArrivalInterval(int minArrivalInterval) {
        this.minArrivalInterval = minArrivalInterval;
    }
    public int getMaxArrivalInterval() {
        return maxArrivalInterval;
    }
    public void setMaxArrivalInterval(int maxArrivalInterval) {
        this.maxArrivalInterval = maxArrivalInterval;
    }
    public int getMinServiceTime() {
        return minServiceTime;
    }
    public void setMinServiceTime(int minServiceTime) {
        this.minServiceTime = minServiceTime;
    }
    public int getMaxServiceTime() {
        return maxServiceTime;
    }
    public void setMaxServiceTime(int maxServiceTime) {
        this.maxServiceTime = maxServiceTime;
    }
    public int getNrOfClients() {
        return nrOfClients;
    }
    public void setNrOfClients(int nrOfClients) {
        this.nrOfClients = nrOfClients;
    }
    public int getNrOfServers() {
        return nrOfServers;
    }
    public void setNrOfServers(int nrOfServers) {
        this.nrOfServers = nrOfServers;
    }

}
