package controllers;

import model.Server;
import model.Simulation;
import utilities.Time;
import views.ResultsView;
import views.SimulationView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import static java.lang.Thread.sleep;

/**
 * Created by Ioan on 03.04.2017.
 */
public class SimulationController implements Runnable {

    InitializationController initializationController;
    Simulation simulation;
    SimulationView simulationView;
    ResultsView resultsView;

    public SimulationController(InitializationController initializationController) {
        this.initializationController = initializationController;
        this.simulationView = new SimulationView();
        this.simulationView.addStopListener(new StopListener());
        this.resultsView = new ResultsView();
    }

    @Override
    public void run() {
        while (!initializationController.isDataGathered())
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        this.initializationController.setInitializationView(false);
        this.simulation = new Simulation(
                initializationController.getTotalSimulationTime(),
                initializationController.getMinArrivalInterval(),
                initializationController.getMaxArrivalInterval(),
                initializationController.getMinServiceTime(),
                initializationController.getMaxServiceTime(),
                initializationController.getNrOfClients(),
                initializationController.getNrOfServers());

        System.out.println(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Simulation created.");
        System.out.println(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Total simulation time: " + initializationController.getTotalSimulationTime());
        System.out.println(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Number of min arrival interval: " + initializationController.getMinArrivalInterval());
        System.out.println(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Number of max arrival interval: " + initializationController.getMaxArrivalInterval());
        System.out.println(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Number of min service time: " + initializationController.getMinServiceTime());
        System.out.println(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Number of max service time: " + initializationController.getMaxServiceTime());
        System.out.println(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Number of clients: " + initializationController.getNrOfClients());
        System.out.println(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Number of servers: " + initializationController.getNrOfServers());

        this.simulation.appendToLog(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Simulation created.\n");
        this.simulation.appendToLog(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Total simulation time: " + initializationController.getTotalSimulationTime() + "\n");
        this.simulation.appendToLog(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Number of min arrival interval: " + initializationController.getMinArrivalInterval() + "\n");
        this.simulation.appendToLog(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Number of max arrival interval: " + initializationController.getMaxArrivalInterval() + "\n");
        this.simulation.appendToLog(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Number of min service time: " + initializationController.getMinServiceTime() + "\n");
        this.simulation.appendToLog(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Number of max service time: " + initializationController.getMaxServiceTime() + "\n");
        this.simulation.appendToLog(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Number of clients: " + initializationController.getNrOfClients() + "\n");
        this.simulation.appendToLog(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Number of servers: " + initializationController.getNrOfServers() + "\n");
        this.simulationView.setLog(simulation.getLog());

        this.simulationView.initialize(initializationController.getNrOfServers());
        this.simulationView.setVisible(true);
        Thread thread = new Thread(this.simulation);
        thread.start();

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    updateQueues(simulation.getServers());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread1.start();

        while (!simulation.isStopped()) {
            this.resultsView.setVisible(false);
            this.resultsView.setResults(simulation.getResults());
        }
        this.simulationView.setVisible(false);
        this.resultsView.setVisible(true);
        try{
            PrintWriter writer = new PrintWriter("log.txt", "UTF-8");
            writer.println(simulation.getLog());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void updateQueues(ArrayList<Server> servers) throws InterruptedException {
        while (!simulation.isStopped()) {
            Thread.sleep(500);
            this.simulationView.updateQueues(servers);
            System.out.println(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Updated queues.");
            simulation.appendToLog(Time.intToString(Time.getTime()) + " SIMULATION CONTROLLER: Updated queues.\n");
            this.simulationView.setLog(simulation.getLog());
        }
    }

    public class StopListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            simulationView.setVisible(false);
            simulation.stopSimulation();
        }
    }

}
