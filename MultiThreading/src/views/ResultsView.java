package views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by Ioan on 10.04.2017.
 */
public class ResultsView extends JFrame {
    JPanel panel;
    JScrollPane scrollPane;
    JTextArea results;

    public ResultsView() {
        this.panel = new JPanel();
        this.panel.setLayout(new GridLayout(1,1));
        this.panel.setBorder(new EmptyBorder(20,20,20,20));
        this.results = new JTextArea();
        this.scrollPane = new JScrollPane(results);
        this.panel.add(scrollPane);
        this.add(panel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(new Dimension(600,450));
    }

    public void setResults(String results) {
        this.results.setText(results);
    }
}
