package views;

import model.Client;
import model.Server;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by Ioan on 03.04.2017.
 */
public class SimulationView extends JFrame {

    private int nrOfServers;
    private JPanel initPanel;
    private JPanel dataPanel;
    private JPanel totalPanel;
    private JPanel panel;
    private JPanel entities[][];
    private JScrollPane data;
    private JTextArea log;
    private JLabel servers[];
    private JLabel clients[][];
    private JButton stop;
    private ImageIcon client;
    private ImageIcon server;

    public SimulationView() {

        this.client = new ImageIcon("F:\\Work\\FACULTATE\\AN 2\\Semestrul 2\\PT\\pt2017_30422_fodorut_ioan\\MultiThreading\\src\\views\\images\\clientmic.png");
        this.server = new ImageIcon("F:\\Work\\FACULTATE\\AN 2\\Semestrul 2\\PT\\pt2017_30422_fodorut_ioan\\MultiThreading\\src\\views\\images\\cashiermic.png");
        this.stop = new JButton("Stop simulation!");
        this.entities = new JPanel[6][];
        this.clients = new JLabel[5][];
        this.initPanel = new JPanel();
        this.dataPanel = new JPanel();
        this.totalPanel = new JPanel();
        this.panel = new JPanel();
        this.log = new JTextArea();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public void initialize(int nrOfServers) {

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) screenSize.getWidth() - 400;
        int height = (int) screenSize.getHeight() - 35;
        this.nrOfServers = nrOfServers;
        initPanel.setLayout(new GridLayout(0, nrOfServers));

        this.setSize(new Dimension(width, height));

        entities = new JPanel[6][nrOfServers];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < nrOfServers; j++) {
                entities[i][j] = new JPanel();
                entities[i][j].setLayout(new GridLayout(0, 1));
            }
        }

        servers = new JLabel[nrOfServers];

        for (int i = 0; i < nrOfServers; i++) {
            servers[i] = new JLabel("Server " + (i + 1), server, JLabel.CENTER);
            servers[i].setVerticalTextPosition(JLabel.TOP);
            servers[i].setHorizontalTextPosition(JLabel.CENTER);
            entities[0][i].add(servers[i]);
            entities[0][i].setVisible(true);
        }

        clients = new JLabel[5][nrOfServers];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < nrOfServers; j++) {
                clients[i][j] = new JLabel("Client", client, JLabel.CENTER);
                clients[i][j].setVerticalTextPosition(JLabel.TOP);
                clients[i][j].setHorizontalTextPosition(JLabel.CENTER);
                entities[i+1][j].add(clients[i][j]);
                entities[i+1][j].setVisible(false);
            }
        }

        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < nrOfServers; j++) {
                initPanel.add(entities[i][j]);
            }
        }

        dataPanel.setLayout(new GridLayout(0, 2));
        panel.add(stop);
        panel.setBorder(new EmptyBorder(30, 20, 10, 20));
        data = new JScrollPane(log);
        data.setBorder(new EmptyBorder(10,20,20,20));
        dataPanel.add(panel);
        dataPanel.add(data);

        totalPanel.setLayout(new BoxLayout(totalPanel, BoxLayout.PAGE_AXIS));
        initPanel.setPreferredSize(new Dimension(width,9*(height-100)/10));
        initPanel.setBorder(new EmptyBorder(20,20,0,20));
        dataPanel.setPreferredSize(new Dimension(width,(height-100)/10));
        totalPanel.add(initPanel);
        totalPanel.add(dataPanel);
        this.add(totalPanel);
    }

    public void updateQueues(ArrayList<Server> servers) {
        for (int i = 0; i < nrOfServers; i++) {
            Client[] clients1 = servers.get(i).getQueue().toArray(new Client[servers.get(i).getQueue().size()]);
            for (int j = 0; j < clients1.length; j++) {
                clients[j][i].setVisible(true);
                clients[j][i].setText(clients1[j].getName());
                entities[j+1][i].setVisible(true);
            }
            for (int j = clients1.length; j < 5; j++) {
                clients[j][i].setVisible(false);
                entities[j+1][i].setVisible(false);
            }
        }
    }

    public int getNrOfServers() {
        return nrOfServers;
    }

    public void setNrOfServers(int nrOfServers) {
        this.nrOfServers = nrOfServers;
    }

    public void setLog(String log) {
        this.log.setText(log);
    }

    public void addStopListener(ActionListener listenForStopButton){
        stop.addActionListener(listenForStopButton);
    }


}
