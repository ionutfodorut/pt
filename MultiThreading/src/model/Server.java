package model;

import utilities.NameFactory;
import utilities.Time;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Ioan on 03.04.2017.
 */

public class Server implements Runnable {

    private BlockingQueue<Client> queue;
    private String name;
    private int id;
    //private int absoluteTime;
    private int freeTime;
    private int waitingTime;
    private int clientsServed;
    private volatile boolean exit = false;

    public Server(int id) {
        this.queue = new ArrayBlockingQueue<>(5);
        NameFactory nameFactory = new NameFactory();
        this.name = nameFactory.generateName();
        this.id = id;
        this.freeTime = 0;
        this.waitingTime = 0;
        this.clientsServed = 0;
    }

    public int getQueueSize() {
        return queue.size();
    }
    public void stopServer() {
        exit = true;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getFreeTime() {
        return freeTime;
    }
    public void setFreeTime(int freeTime) {
        this.freeTime = freeTime;
    }
    public BlockingQueue<Client> getQueue() { return queue; }
    public int getWaitingTime() {
        return waitingTime;
    }
    public int getClientsServed() { return clientsServed; }
    public void setQueue(BlockingQueue<Client> queue) {
        this.queue = queue;
    }
    void assignClient(Client c) throws InterruptedException { queue.put(c); }
    Client serveClient() throws InterruptedException {
        return queue.take();
    }

    @Override
    public void run() {

        while (!exit) {
            if (!queue.isEmpty()) {
                try {
                    Client c = queue.peek();
                    if (getQueueSize() > 1) {
                        waitingTime += c.getServiceTime();
                    }
                    Thread.sleep(c.getServiceTime() * 1000);
                    serveClient();
                    clientsServed++;
                    Simulation.updateAbsoluteTime(c.getServiceTime());
                    System.out.println(Time.intToString(Simulation.getAbsoluteTime()) + " SERVER " + this.id + ":" + " Client " + c.getName() + ", with ID " + c.getId() + ", has been succesfully served for " + c.getServiceTime() + " seconds.\n");
                    Simulation.appendToLog(Time.intToString(Simulation.getAbsoluteTime()) + " SERVER " + this.id + ":" + " Client " + c.getName() + ", with ID " + c.getId() + ", has been succesfully served for " + c.getServiceTime() + " seconds.\n");
                    if (getQueueSize() == 0) {
                        System.out.println(Time.intToString(Simulation.getAbsoluteTime()) + " SERVER " + this.id + ":" + " Queue is now empty!\n");
                        Simulation.appendToLog(Time.intToString(Simulation.getAbsoluteTime()) + " SERVER " + this.id + ":" + " Queue is now empty!\n");
                    } else {
                        System.out.println(Time.intToString(Simulation.getAbsoluteTime()) + " SERVER " + this.id + ":" + " Client to be served is " + queue.peek().getName() + ", with ID " + queue.peek().getId() + ", Queue size is now " + getQueueSize() + ".\n");
                        Simulation.appendToLog(Time.intToString(Simulation.getAbsoluteTime()) + " SERVER " + this.id + ":" + " Client to be served is " + queue.peek().getName() + ", with ID " + queue.peek().getId() + ", Queue size is now " + getQueueSize() + ".\n");
                    }

                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    this.freeTime++;
                    Thread.sleep(1000);
                    System.out.println(Time.intToString(Simulation.getAbsoluteTime() + freeTime) + " SERVER " + this.id + ": Slept for 1 second\n");
                    Simulation.appendToLog(Time.intToString(Simulation.getAbsoluteTime() + freeTime) + " SERVER " + this.id + ": Slept for 1 second\n");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
