package utilities;

import model.Client;

import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Math.random;

/**
 * Created by Ioan on 03.04.2017.
 */
public class ClientFactory {
    private int maxServiceTime;
    private int minServiceTime;

    public ClientFactory(int minServiceTime, int maxServiceTime) {
        this.minServiceTime = minServiceTime;
        this.maxServiceTime = maxServiceTime;
    }

    public Client getClient() {
        NameFactory nameFactory = new NameFactory();
        int serviceTime = ThreadLocalRandom.current().nextInt(minServiceTime, maxServiceTime + 1);
        return new Client(nameFactory.generateName(), serviceTime);
    }
}
